output_names = ['dense_1/Softmax']
input_names = ['conv2d_input']

import tensorflow as tf

labels_list = ['Speed limit (20km/h)',
               'Speed limit (30km/h)',
               'Speed limit (50km/h)',
               'Speed limit (60km/h)',
               'Speed limit (70km/h)',
               'Speed limit (80km/h)',
               'Speed limit (100km/h)',
               'Speed limit (120km/h)',
               'Yield',
               'Stop',
               'No entry',
               'Road work',
               'Pedestrians',
               'Turn right ahead',
               'Turn left ahead',
               'Ahead only']




def get_frozen_graph(graph_file):
    """Read Frozen Graph file from disk."""
    with tf.io.gfile.GFile(graph_file, "rb") as f:
        graph_def = tf.compat.v1.GraphDef()
        graph_def.ParseFromString(f.read())
    return graph_def


trt_graph = get_frozen_graph('trt_graph.pb')

# Create session and load graph
tf_config = tf.compat.v1.ConfigProto()
tf_config.gpu_options.allow_growth = True
tf_sess = tf.compat.v1.Session(config=tf_config)
tf.import_graph_def(trt_graph, name='')


# Get graph input size
for node in trt_graph.node:
    print("-----------------"+node.name)
    if 'conv2d_' in node.name:
        size = node.attr['shape'].shape
        image_size = [size.dim[i].size for i in range(1, 4)]
        break
print("image_size: {}".format(image_size))


# input and output tensor names.
input_tensor_name = input_names[0] + ":0"
output_tensor_name = output_names[0] + ":0"

print("input_tensor_name: {}\noutput_tensor_name: {}".format(
    input_tensor_name, output_tensor_name))

output_tensor = tf_sess.graph.get_tensor_by_name(output_tensor_name)

#from tensorflow.keras.preprocessing import image
#from tensorflow.keras.applications.mobilenet_v2 import preprocess_input, decode_predictions
import numpy as np
import cv2
# Optional image to test model prediction.

def predict(img):
    img = cv2.resize(img, (32, 32))
    img_to_pred = img.reshape(1, 32, 32, 3)

    feed_dict = {
        input_tensor_name: img_to_pred
    }
    preds = tf_sess.run(output_tensor, feed_dict)


    return labels_list[np.argmax(preds[0])]



"""import time
times = []
for i in range(20):
    start_time = time.time()
    one_prediction = tf_sess.run(output_tensor, feed_dict)
    delta = (time.time() - start_time)
    times.append(delta)
mean_delta = np.array(times).mean()
fps = 1 / mean_delta
print('average(sec):{:.2f},fps:{:.2f}'.format(mean_delta, fps))

"""
