import numpy as np
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense
from keras.layers import Dropout, BatchNormalization
from keras.optimizers import SGD,Adam
from keras.regularizers import l2
from keras.preprocessing.image import ImageDataGenerator



def build_model(n_labels):
    # Initialisation du modèle
    model = Sequential()

    #Notre réseau
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(MaxPooling2D((2, 2)))

    # Etage de classification
    model.add(Flatten())
    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dropout(0.2))

    model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
    model.add(Dropout(0.2))

    model.add(Dense(n_labels, activation='softmax'))

    opt = SGD(lr=0.0001, momentum=0.9)

    model.compile(optimizer="adam",
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model

def build_model1(n_labels):
    model = Sequential()

    model.add(Conv2D(32, (5, 5), activation='relu', input_shape=(32, 32, 3)))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(64, (3, 3), activation='relu'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))

    model.add(Dense(n_labels, activation='softmax'))

    #opt = SGD(lr=0.0001, momentum=0.9)
    #opt = Adam(lr=INIT_LR, decay=INIT_LR / (NUM_EPOCHS * 0.5))
    # compiler
    model.compile(optimizer="adam",
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])

    return model



def train_model(model, X_train, y_train, X_test, y_test, n_epochs, batch_size, classWeight):
    h = model.fit(X_train, y_train, class_weight=classWeight, epochs=n_epochs, batch_size=batch_size,
                  validation_data=(X_test, y_test))  # , verbose=0)

    return h


def save_model(model, path, name):
    model.save(path + name)
