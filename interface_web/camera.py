#Import necessary libraries
from flask import Flask, render_template, Response
import cv2
import random
import datetime, time


from flask_socketio import SocketIO, emit

app = Flask(__name__)
socketio = SocketIO(app)

camera = cv2.VideoCapture(0)


def gen_frames():
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            break
        else:
            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@socketio.on('connect')
def test_connect():
    emit('after connect',  {'data':'Lets dance'})

@socketio.on('server')
def send_temp():
    while True:
        d = datetime.datetime.utcnow()
        for_js = int(time.mktime(d.timetuple())) * 1000
        emit('temp',  {"date":for_js, "temp": random.random()*10})
        socketio.sleep(1)


if __name__ == "__main__":
    #app.run(debug=True, host="localhost", port=3333)
    socketio.run(app, host='0.0.0.0', port=3333, debug=True)

