# ML-Projet

Projet de machine learning S8

Le but de ce projet est de creer un reseau de neuronne permettant de classifier des panneaux de la route.

L'idee de ce projet nous est venue dans une optique de robotique dans l'idee de pouvoir possiblement controler un petit robot par des panneaux positionnées sur une piste de robot, ex : PFM. 

Au dela de l'aspect robotique c'est interessant car le code de la route et la conduite est un milieu ou l'IA est en plein essort. Elle pourrait permettre (sous reserve de tres bonne fiabilite) de reduire les accidents de la route et ainsi de proposer un moyen de transport plus sur aux populations.


# Setup 

Pour cloner le reportoire 
>$ git clone https://gitlab.com/DamienCM/ml-projet.git

Pour initialiser le venv
>$ python3 -m venv venv

Pour activre le venv :
* Windows
    >$ . venv/Scripts/activate.ps1
* Linux 
    >$ source venv/bin/activate

Pour installer les dependences 
>(venv)$ pip install -r requirements.txt

# Code de la Jetson
Le code utilisé pour déployer et tester le modèle sur la Jetson se trouve dans le dossier [jetson](jetson). Nous avons utilisé ROS afin d'acquérir
le flux caméra les détails pour la reproduction se trouvent ici: [Lien](https://github.com/dusty-nv/jetbot_ros)

Notez que le fichier ".h5" et le fichier ".pb" (après passage par TensorRT) sont fournis

# Interface web
Le code pour le serveur Flask ainsi que le code du Frontend se trouvent dans le dossier [interface_web](interface_web)

# Demo 1
[![demo1](https://yt-embed.herokuapp.com/embed?v=FkK85ea4Wcg)](https://www.youtube.com/watch?v=FkK85ea4Wcg-Y "Everything Is AWESOME")


# Demo 2
[![demo1](https://yt-embed.herokuapp.com/embed?v=Z7OcZ4ziz-o)](https://www.youtube.com/watch?v=Z7OcZ4ziz-o "Everything Is AWESOME")


