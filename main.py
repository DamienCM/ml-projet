import numpy as np 
import matplotlib.pyplot as plt
import tensorflow as tf
import keras
import pandas
import cv2
import os
import tarfile
import IPython 

# Extracting the dataset 

path_to_extract = 'data_set_extracted'
if not(os.path.isdir(path_to_extract)):
    print(f'Extracting data set to : {path_to_extract}')
    tar = tarfile.open('traffic_sign_data_set.tar')
    tar.extractall(path_to_extract)
    tar.close()
else :
    print('Dataset already extracted')


# Definition des directories des donnees
test_dir_images = f'{path_to_extract}/Test'
test_file = f'{path_to_extract}/Test.csv'
train_dir_images = f'{path_to_extract}/Train'
train_file = f'{path_to_extract}/Train.csv'


# vérification du nombre de photos dans chaque dossier

print('total training images:', len(os.listdir(train_dir_images)))

print('Nombre de types de panneaux :', len(os.listdir(test_dir_images)))

types_panneaux_str = ['20kmh', '30kmh', '50kmh', '60kmh', '70kmh', '80kmh', '100kmh', '120kmh', 
                    'cedez le passage', 'stop', 'sens interdit', 'travaux', 'pietons', 'droite',
                     'gauche', 'tout droit']

[print(types_panneaux_str[i], f'({i})' , len(os.listdir(f'{train_dir_images}/{i}'))) for i in range(len(os.listdir(train_dir_images)))]


IPython.embed()