#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import cv2, math
from keras.models import load_model

import sys
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from main import process





class image_converter:

    def __init__(self):
        #self.image_pub = rospy.Publisher("image_topic_2", Image, queue_size=2)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/tb3/camera1/image_raw", Image, self.callback)

    def callback(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        #(rows, cols, channels) = cv_image.shape
        # if cols > 60 and rows > 60 :
        # cv2.circle(cv_image, (50,50), 10, 255)
        try:
            cv2.imshow("Image window", process(cv_image))
            cv2.waitKey(3)
        except Exception as e:
            print(e)
            cv2.imshow("Image window", cv_image)
            cv2.waitKey(3)

        """try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)"""

def main(args):
    ic = image_converter()
    rospy.init_node('image_converter', anonymous=True)
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()


if __name__ == '__main__':
    main(sys.argv)
