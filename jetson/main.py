import cv2
import numpy as np
import matplotlib.pyplot as plt
from pres import predict


def color_filter(image):
    cop = image.copy()
    image = cv2.cvtColor(src=image, code=cv2.COLOR_BGR2HSV)
    lower1 = np.array([0, 100, 20])
    upper1 = np.array([10, 255, 255])

    # upper boundary RED color range values; Hue (160 - 180)
    lower2 = np.array([160, 100, 20])
    upper2 = np.array([179, 255, 255])


    lower_blue=np.array([100,50,50])
    upper_blue=np.array([140,255,255])

    lower_mask = cv2.inRange(image, lower1, upper1)
    upper_mask = cv2.inRange(image, lower2, upper2)
    blue_mask = cv2.inRange(image, lower_blue, upper_blue)


    full_mask = lower_mask + upper_mask +blue_mask

    return full_mask, cop


def process(image):
    a, image = color_filter(image)

    contours, hierarchy = cv2.findContours(image=a, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE)

    con = []
    area_list = []
    cont_list = []
    for i, cnt in enumerate(contours):
        area = cv2.contourArea(cnt)
        epsilon = 0.1*cv2.arcLength(cnt,True)
        approx = cv2.approxPolyDP(cnt,epsilon,True)
        area_list.append(area)
        cont_list.append((cnt,cv2.isContourConvex(approx)))


        #print((area,cv2.isContourConvex(approx)))


    m = max(area_list)
    for i, cnt in enumerate(contours):
        area = cv2.contourArea(cnt)
        if area > m * 0.1 and cont_list[i][1]:
            x,y,w,h = cv2.boundingRect(cont_list[i][0])
            cropped_image = image[y:y+h, x:x+w]
            #cv2.imwrite('contours_none_image1.jpg', cropped_image)
            overlay = image.copy()
            cv2.rectangle(overlay,(x,y),(x+w,y+h),(0,255,0),-1)
            image = cv2.addWeighted(overlay, 0.5,image, 0.5, 0)
            cv2.putText(image, predict(cropped_image), (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (36,255,12), 2)
            con.append(cnt)




    contours = con

    return image

#cv2.drawContours(image=image, contours=contours, contourIdx=-1, color=(0, 255, 0), thickness=2, lineType=cv2.LINE_AA)
"""
cv2.imshow('None approximation', image)
cv2.waitKey(0)

cv2.destroyAllWindows()
"""