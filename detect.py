import cv2
import numpy as np
import matplotlib.pyplot as plt
from numpy.core.defchararray import array
from numpy.core.numeric import full


def color_filter(path):
    image = cv2.imread(path)
    copy = image.copy()

    # to hsv
    image = cv2.cvtColor(src=image, code=cv2.COLOR_BGR2HSV)

    # red mask
    redtol = 15
    stol =int(25*255/100)
    vtol = int(25*255/100)
    lower_red = np.array([0, stol, vtol]) #330 a 30 pour le H;   100 a 50 pour le S;  100 a 50 pour le V
    upper_red = np.array([redtol, 255, 255])
    red_mask1 = cv2.inRange(image, lower_red, upper_red)


    lower_red = np.array([180-redtol, stol, vtol]) #330 a 30 pour le H;   100 a 50 pour le S;  100 a 50 pour le V
    upper_red = np.array([360, 255, 255])
    red_mask2=cv2.inRange(image, lower_red, upper_red)

    # cv2.imshow('red1',red_mask1)
    # cv2.imshow('red2',red_mask2)
    red_mask = cv2.bitwise_or(red_mask1,red_mask2)
    # cv2.imshow('redtot',red_mask)

    # blue mask
    lower_blue=np.array([106,50,50])
    upper_blue=np.array([130,255,255])
    blue_mask = cv2.inRange(image, lower_blue, upper_blue)

    # somme des 2 
    full_mask = red_mask  + blue_mask
    
    full_mask = cv2.blur(full_mask,(5,5))

    full_mask = np.where(full_mask>255/3.5,255,0)
    full_mask = cv2.normalize(full_mask, None, 255,0, cv2.NORM_MINMAX, cv2.CV_8UC1)
    cv2.imshow('test',full_mask)
    return full_mask, copy


def fill_holes(image):
    # Read image

    # Threshold.
    # Set values equal to or above 220 to 0.
    # Set values below 220 to 255.


    # Copy the thresholded image.
    im_floodfill = image.copy()

    # Mask used to flood filling.
    # Notice the size needs to be 2 pixels than the image.
    h, w = image.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)

    # Floodfill from point (0, 0)
    cv2.floodFill(im_floodfill, mask, (10,10), 255)

    # Invert floodfilled image
    im_floodfill_inv = cv2.bitwise_not(im_floodfill)

    # Combine the two images to get the foreground.
    im_out = image | im_floodfill_inv

    # # Display images.
    # cv2.imshow("Thresholded Image", image)
    # cv2.imshow("Floodfilled Image", im_floodfill)
    # cv2.imshow("Inverted Floodfilled Image", im_floodfill_inv)
    # cv2.imshow("Foreground", im_out)
    # cv2.waitKey(0)
    return im_out


a, image = color_filter("test/env.PNG")


cv2.imshow("Mask",a)
a = fill_holes(a)
cv2.imshow("Fill holes",a)

contours, hierarchy = cv2.findContours(image=a, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE)

print(hierarchy)

con = []
area_list = []
cont_list = []
# print(len(contours))
a=cv2.cvtColor(src=a,code=cv2.COLOR_GRAY2BGR)
for cnt in contours:
    area = cv2.contourArea(cnt)
    epsilon = 0.1*cv2.arcLength(cnt,True)
    approx = cv2.approxPolyDP(cnt,epsilon,True)
    area_list.append(area)
    cont_list.append((cnt,cv2.isContourConvex(approx)))



crops =[]

min_width = int(len(image)/100)
min_height = int(len(image[0]/100))
max_width = int(len(image)/5)
max_height = int(len(image[0]/5))

min_area = min_height*min_width
max_area=max_width*max_height

print(f'min{min_area}',f'max{max_area}')
print(area_list)

m = max(area_list) # probleme sur une image sans panneau to be ameliorer
for i, cnt in enumerate(contours):
    area = area_list[i]
    if max_area>area > min_area :
        x,y,w,h = cv2.boundingRect(cont_list[i][0])
        crops.append(image[y:y+h, x:x+w])
        cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)
        con.append(cnt)
        print("panneau trouve")


for i,c in enumerate(crops):
    cv2.imshow(f'{i}',c)



cv2.waitKey(0)

cv2.destroyAllWindows()
