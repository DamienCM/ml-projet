from keras.models import load_model
import matplotlib.pyplot as plt
import cv2
import tensorflow

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession

config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)
#tensorflow.keras.backend.set_learning_phase(0)

labels_list = ['Speed limit (20km/h)',
               'Speed limit (30km/h)',
               'Speed limit (50km/h)',
               'Speed limit (60km/h)',
               'Speed limit (70km/h)',
               'Speed limit (80km/h)',
               'Speed limit (100km/h)',
               'Speed limit (120km/h)',
               'Yield',
               'Stop',
               'No entry',
               'Road work',
               'Pedestrians',
               'Turn right ahead',
               'Turn left ahead',
               'Ahead only']

model = load_model("cnn_traffic_sign.h5")
def predict_image(img):

    img = cv2.resize(img, (32, 32))
    img_to_pred = img.reshape(1, 32, 32, 3)

    y_pred = model.predict_classes(img_to_pred)
    label_pred = labels_list[y_pred[0]]

    return label_pred

"""path = "traffic_sign_dataset/"
X_train, y_train, X_test, y_test, labels_list = load_dataset(path)
model_name = "cnn_traffic_sign.h5"
print(predict_imgs(path, model_name, X_test, labels_list))"""